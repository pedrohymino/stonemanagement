// Angular native
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule,Http, RequestOptions } from "@angular/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

// Plugins
import { AppRoutingModule } from "./app-routing.module";
import { CalendarModule } from 'primeng/calendar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MessageService, ConfirmationService } from 'primeng/api';
import {NgxMaskModule} from 'ngx-mask';
import { NgxCurrencyModule } from "ngx-currency";


// SENIOR 
import { HttpClientModule } from '@angular/common/http';


// Pages
import { AppComponent } from "./app.component";
import { OrigensComponent } from "./pages/origens/origens.component";
import { MenuopComponent } from "./pages/menuop/menuop.component";
import { ConsultaloteComponent } from "./pages/consultalote/consultalote.component";
import { ApontamentoindividualComponent } from './pages/apontamentoindividual/apontamentoindividual.component';
// Cadastros
import { PerfilComponent } from './pages/cadastros/perfil/perfil.component';
import { RelacionamentoComponent } from './pages/cadastros/relacionamentoperfil/relacionamento.component';

// Services
import { ApiService } from "./services/api/api.service";
import { GlobalService } from "./services/global/global.service";
import { SelecionaEmpresaComponent } from './pages/seleciona-empresa/seleciona-empresa.component';


@NgModule({
  declarations: [
    AppComponent,
    OrigensComponent,
    MenuopComponent,
    ConsultaloteComponent,
    ApontamentoindividualComponent,
    PerfilComponent,
    RelacionamentoComponent,
    SelecionaEmpresaComponent,
  ],
  imports: [
    HttpModule,
    BrowserModule, BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,

    // Senior
    HttpClientModule,

    NgxMaskModule.forRoot(),
    NgxCurrencyModule,

    // PrimeNG
    CalendarModule,
    ScrollPanelModule,
    ToastModule,
    ConfirmDialogModule,
    ProgressSpinnerModule
  ],
  providers: [
    ApiService,
    GlobalService,
    // PrimeNG
    MessageService,
    ConfirmationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
