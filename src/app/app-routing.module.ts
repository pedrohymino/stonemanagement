import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GlobalService } from './services/global/global.service';
import { AppService } from './senior/services/app.service';

/* PAGES*/
import { OrigensComponent } from './pages/origens/origens.component';
import { MenuopComponent } from './pages/menuop/menuop.component';
import { ConsultaloteComponent } from './pages/consultalote/consultalote.component';
import { ApontamentoindividualComponent } from './pages/apontamentoindividual/apontamentoindividual.component';
// Cadastros
import { PerfilComponent } from './pages/cadastros/perfil/perfil.component';
import { RelacionamentoComponent } from './pages/cadastros/relacionamentoperfil/relacionamento.component';

const routes: Routes = [
  // { 
  //   path: '', 
  //   // redirectTo: '/origens', 
  //   pathMatch: 'full',
  // },
  { 
    path: 'origens',
    component: OrigensComponent
  },
  
  { 
    path: 'menuop/:id/:codorigem',
    component: MenuopComponent,
    canActivate: [GlobalService]
  },

  { 
    path: 'consultalote/:codori/:id/:prod/:lastpage',
    component: ConsultaloteComponent,
    canActivate: [GlobalService]
  },

  { 
    path: 'apotamentoindividual/:codori/:id/:prod/:lastpage',
    component: ApontamentoindividualComponent,
    canActivate: [GlobalService]
  },
  
  // Cadastros
  {
    path: 'cadastro/perfil',
    component: PerfilComponent,
  },
  {
    path: 'cadastro/relacionamento',
    component: RelacionamentoComponent,
  },


  // { path: 'detail/:id', component: HeroDetailComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
