import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/timeout";
import { Observable, Subject } from "rxjs";

// const API_URL = "https://swapi.co/api/people/";
// https://restcountries.eu/rest/v2/name/eesti

const API_URL = "http://192.168.0.50:9020/apontamento/api/1.0.0/"

//Local
//const API_URL = "http://plutaodes:9020/apontamento/api/1.0.0/";

//PlutaoDES
//const API_URL = "http://dev.mdsistemas.com.br:9020/apontamento/api/1.0.0/";



@Injectable()
export class ApiService {
  empresa$: Observable<boolean>;
  public emp = new Subject<boolean>();

  objCtrl = {
    empresaSelect: {id:0},
    User: '',
    Senha: '',
    UserSenior: '',
    Tenant: ''
  }

  listEmpresas = [];

  listDefeitos;

  listQualidades;

  tokenapi;

  constructor(
    private http: Http
  ) {

    this.emp = new Subject<boolean>();
    this.empresa$ = this.emp.asObservable();
    
  }


  saveDateSeniorX(dados){
    return new Promise((resolve, reject) => {
      this.objCtrl.UserSenior = dados.nome;
      this.objCtrl.Tenant = dados.tenantName;
      resolve(true);
    });
  }

  getOrigens({NomeUser,CodEmp,Tenant}) {
    var promise = new Promise((resolve, reject) => {
      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}origem/listar/usuario?nomeUsuario=${NomeUser}&codEmpresa=${CodEmp}&tenantName=${Tenant}`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          //console.log('lista v ',data);
          resolve(data);
        },
        err => {
          reject(err);
        }
      );
    })
    return promise;
  }

  getOrigensGeral({NomeUser,CodEmp,Tenant}) {
    var promise = new Promise((resolve, reject) => {
      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}origem/listar?nomeUsuario=${NomeUser}&codEmpresa=${CodEmp}&tenantName=${Tenant}`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          resolve(data);
        },
        err => {
          reject(err);
        }
      );
    })
    return promise;
  }

  generateAssociacaoOrigens(codEmp,tenant,listaOrigens: any): Promise<any> {
   
    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${API_URL}perfil/origem/associar?codEmpresa=${codEmp}&tenantName=${tenant}`,listaOrigens, options).toPromise()
      .then((res) => {
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err)
      })

  }

  generateAssociacaoPerfis(codEmp,tenant,listaPerfis: any): Promise<any> {
   
    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${API_URL}usuario/perfil/salvar?codigoEmpresa=${codEmp}&tenantName=${tenant}`,listaPerfis, options).toPromise()
      .then((res) => {
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err.json())
      })

  }


  getOps({User,Senha,UserSenior,CodOri,CodEmp,Tenant}) {
    return new Promise((resolve, reject) => {
      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}op/listar?usuario=${User}&senha=${Senha}&usuarioSenior=${UserSenior}`+
      `&codigoOrigem=${CodOri}&codigoEmpresa=${CodEmp}&tenantName=${Tenant}`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          if(data.listaOP == null){
            resolve(null);
          }else{
            // console.log('listaOP v ',data.listaOP);
            resolve(data.listaOP);
          }
        },
        err => {
          reject(err);
        }
      );
    });
  }
  
  
  getLote({User,Senha,UserSenior,NumOP,CodEmp,CodOri,CodProd,Tenant}) {
    return new Promise((resolve, reject) => {
      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}lote/listar?usuario=${User}&senha=${Senha}&usuarioSenior=${UserSenior}`+
      `&numeroOP=${NumOP}&codigoEmpresa=${CodEmp}&codigoOrigem=${CodOri}&codigoProduto=${CodProd}&tenantName=${Tenant}`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          resolve(data);
        },
        err => {
          reject(err);
        }
      );
    });
  }

  //OLD usado apenas para TESTE qndo nao está com as calls da senior X
  getEmpresas(){
    return new Promise((resolve, reject) => {
      this.http.get(`https://randomuser.me/api/?results=6`).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          // console.log(data);
          // console.log(data.results);
          data.results.filter((item,index) => {
            let emp: empresa = {
              id: index+1,
              name: `${item.name.first} ${item.name.last}`,
              selected: false
            }
            this.listEmpresas.push(emp);
          });
          resolve(this.listEmpresas);
        },
        err => {
          reject(err);
        }
      );
    });
  }


  autenticaMD(){
    return new Promise((resolve, reject) => {
      
      let headers = new Headers({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Authorization, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X-Custom-header'
      });

      let param_full = {
        "username":"senior-es-dev",
        "password":"senior-es-dev"
      };
      let body = JSON.stringify(param_full);
      let options = new RequestOptions({headers: headers, body: body});
      
      this.http.post(`${API_URL}login`, body, options).timeout(30000)
      .subscribe(
        data => {
          let token = data.headers.get('Authorization');
          // console.log(token);
          this.setToken(token);
          resolve(true);
        },
        err => {
          reject(err);
        }
      );

    });
  }

  autenticaSeniorX(){
    return new Promise((resolve, reject) => {

      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}autenticar/usuario?usuarioSenior=thymoteo.martins&tenantName=senior-es-dev`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          //console.log(data);
          data.listaEmpresa.filter((item,index) => {
            let emp: empresa = {
              id: item.codigoEmpresa,
              name: item.nomeEmpresa,
              selected: false
            }
            this.listEmpresas.push(emp);

            // andre - pega retorno do usario e senha
            this.objCtrl.User = data.loginErp;
            this.objCtrl.Senha = data.senhaErp;


          });
          //console.log('this.objCtrl',this.objCtrl);
          //console.log('this.listEmpresas',this.listEmpresas);
          resolve(this.listEmpresas);
        },
        err => {
          reject(err);
        }
      );
    });
  }



  generateDefeitos({CodEmp,CodOri,UserSenior,Tenant}) {
    return new Promise((resolve, reject) => {

      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}defeito/listar?codigoEmpresa=${CodEmp}&codigoOrigem=${CodOri}`+
      `&usuarioSenior=${UserSenior}&tenantName=${Tenant}`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          this.listDefeitos = data.defeitos
          resolve(true); //precisa retornar os defeitos, por enquanto esta voltando uma lista vazia!
        },
        err => {
          reject(err);
        }
      );
    });
  }


  getDefeitos(){
    return new Promise((resolve)=>{
      //let waiting = setInterval(() => {
        if(this.listDefeitos.length > 0 ){
          //clearInterval(waiting);
          resolve(this.listDefeitos);
        }
      //},100)
    });
  }

  generateQualidade({CodEmp,CodOri,UserSenior,Tenant}) {
    return new Promise((resolve, reject) => {

      let headers = new Headers({'Authorization': this.getToken()});
      this.http.get(`${API_URL}qualidade/listar?codigoEmpresa=${CodEmp}&codigoOrigem=${CodOri}`+
      `&usuarioSenior=${UserSenior}&tenantName=${Tenant}`,{headers: headers}).timeout(30000)
      .map(res => res.json()).subscribe(
        data => {
          this.listQualidades = data.qualidades
          
          resolve(this.listQualidades); 
        },
        err => {
          reject(err);
        }
      );
    });
  }

  generateApontamentoInicioFim(codEmp,tenant,objApontamento: any): Promise<any> {
   
    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${API_URL}apontar/iniciofim?codEmpresa=${codEmp}&tenantName=${tenant}`,objApontamento, options).toPromise()
      .then((res) => {
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err.json())
      })

  }
  
  generateApontamentoLotes(codEmp,tenant,objApontamento: any): Promise<any> {

    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(`${API_URL}apontar/lote?codEmpresa=${codEmp}&tenantName=${tenant}`,objApontamento, options).toPromise()
      .then((res) => {
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err.json())
      })

  }

  getPerfil(codEmp,tenant): Promise<any> {
   
    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${API_URL}perfil/listar?codEmpresa=${codEmp}&tenantName=${tenant}`, options).toPromise()
      .then((res) => {
        
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err)
      })

  }

  getUsuario(codEmp,tenant): Promise<any> {
   
    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${API_URL}usuario/listar?codigoEmpresa=${codEmp}&tenantName=${tenant}`, options).toPromise()
      .then((res) => {
        
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err)
      })

  }

  getPerfisUsuario(codEmp,idUsuario,tenant): Promise<any> {
   
    let headers = new Headers({'Authorization': this.getToken()});
    let options = new RequestOptions({ headers: headers });

    return this.http.get(`${API_URL}perfil/listar/usuario?codEmpresa=${codEmp}&idUsuario=${idUsuario}&tenantName=${tenant}`, options).toPromise()
      .then((res) => {
        
        return res.json()
      })
      .catch((err) => {
        return Promise.reject(err)
      })

  }

  getCurrentNameEmpresa(){
    let empActive = 'Selecionar';
    this.listEmpresas.filter(item=>{
      if(item.selected){
        empActive = item.name;
      }
    })
    return empActive;
  }

  getCurrentEmpresa(){
    let empActive = this.resetIntEmpresa();
    
    this.listEmpresas.filter(item=>{
      if(item.selected){
        this.objCtrl.empresaSelect = item;
        empActive = item;
      }
    });
    return empActive;
  }


  changeEmpresa(idEmpresa){
    //this.emp.next(true); andre

    this.listEmpresas.filter(item=>{
      if(item.id == idEmpresa){
        this.objCtrl.empresaSelect = item;

        // andre: 
        this.emp.next(true);

        item.selected = true;
      }else{
        item.selected = false;
      }
    });
  }


  resetIntEmpresa(){
    let ax:empresa = {
      id: 0,
      name: null,
      selected: false
    };
    return ax;
  }



  getToken(){
    return this.tokenapi;
  }
  setToken(newToken){
     this.tokenapi = newToken;
  }

}

export interface empresa {
  id: number,
  name: string,
  selected: boolean
}
