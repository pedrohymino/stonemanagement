import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { MessageService, ConfirmationService } from 'primeng/api';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiService } from '../api/api.service';

declare var $: any;

@Injectable()
export class GlobalService {

    callSenior = false; // habilita ou desabilitaa chamada senior X [false = teste] & [true = produção]
    //callSenior = true; // habilita ou desabilitaa chamada senior X [false = teste] & [true = produção]

    opSelecionada = []

    constructor(
        public api: ApiService,
        private messageService: MessageService,
        private confirmService: ConfirmationService,
    ) {
    }

    localeCalendar(){
        return {
            firstDayOfWeek: 0,
            dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
            dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
            dayNamesMin: ["D","S","T","Q","Q","S","S"],
            monthNames: [ "Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro" ],
            monthNamesShort: [ "Jan", "Fev", "Mar", "Abr", "Mai", "Jun","Jul", "Ago", "Set", "Out", "Nov", "Dez" ],
        };
    }

    randomNum(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    addZeroBefore(qtdNumbers,val){
        let zeros='';
        // exemplo ('00' + 421).slice(-3)
        /*##############################################
            Se a quantidade de casas do valor for 
            maior que a quantidade máxima informada
            qtdNumbers recebe a quantidade de casas
            do valor informado!
        ##############################################*/ 
        if(qtdNumbers<val.toString().length){
            qtdNumbers = val.toString().length
          }
        for(var i=0; i<qtdNumbers; i++){
          zeros +='0';
        }
        return (zeros + val).slice(-qtdNumbers);
    }

    getDate(data){
        return moment(data).format('DD/MM/YYYY');
    }
    getHour(data){
        return moment(data).format('HH:mm');
    }


    // # type
    // success | info | warn | error
    // # position
    // top-right | top-left | bottom-right | bottom-left | top-center | bottom-center | center
    showToast({type, title='', text='', position='top-right', life=3000}){
        this.messageService.add({key: position, severity:type, summary: title, detail: text, life});
    }

    showConfirm({header, message, icon=''}){
        return new Promise((resolve) => {
            this.confirmService.confirm({
                header,
                message,
                icon,
                accept: () => {
                    resolve(true);
                },
                reject: () => {
                    resolve(false);
                }
            });
        });
    }

    closeToast(event){
      // hide p-toastitem
      event.path.filter(item=>{
        if(item.tagName != undefined){
          if(item.tagName.toLowerCase() == 'p-toastitem'){
            $(`.${item.classList[0]}`).hide();
          }
        }
      });
    }

    registraOp(opSelecionada) {
        this.opSelecionada = opSelecionada
    }

    exibeOpSelecionada() {
        return this.opSelecionada
    }

    canActivate(route ?: ActivatedRouteSnapshot, state ?: RouterStateSnapshot): boolean {
      if(this.api.getCurrentEmpresa().id == 0){
        return false;
      }else{
        return true
      }
    }

}
