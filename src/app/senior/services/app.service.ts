import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
//componentes que obtém os dados de autenticação da plataforma
import { service, user } from '@seniorsistemas/senior-platform-data';

import { GlobalService } from '../../services/global/global.service';

import { Observable } from 'rxjs/Rx';

@Injectable()
export class AppService {

  constructor(
    private http: HttpClient,
    public global: GlobalService
  ) {
    if(this.global.callSenior){
      this.getDataPromise();
    }
  }

  getDadosUsuario() : Observable < any > {
    return Observable.forkJoin(
      //busca a url base e o token para montagem do request.
      Observable.fromPromise(service.getServicesUrl()),
      Observable.fromPromise(service.getRestUrl()),
      Observable.fromPromise(user.getAuthHeader()),
    ).flatMap((values: any) => {      
      const [serv, bridgeUrl, authHeader] = values;
      let headers = new HttpHeaders({
        "Authorization": authHeader
      });
      return this.http.get < any > (`${serv}usuarios/userManager/queries/obterMeusDados`,{headers});
    });
  }
  

  getDataPromise() {
    let dataPromise;
    if (!dataPromise) {
        console.warn("Senior Platform data not found. Waiting for message event...");
        dataPromise = new Promise((resolve, reject) => {
            setTimeout(() => {
                reject(new Error("It was not possible to retrieve the Senior Platform data"));
            }, 5000);
            window.addEventListener("message", evt => {
                if (evt.data && evt.data.token && evt.data.token.username && evt.data.token.token_type) resolve(evt.data);
            });
        });
    }

    return dataPromise;
  }

}

export function getServicesUrl() {
  return this.getDataPromise().then(data => data.servicesUrl);
}

export function getToken() {
  return this.getDataPromise().then(data => data.token);
}


