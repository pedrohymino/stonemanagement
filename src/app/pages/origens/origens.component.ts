import { Component, OnInit } from "@angular/core";
import { GlobalService } from "../../services/global/global.service";
import { ApiService, empresa } from "../../services/api/api.service";

import { SelecionaEmpresaComponent } from '../seleciona-empresa/seleciona-empresa.component';
///import { setTimeout } from "timers";

declare var $: any;

@Component({
  selector: "app-origens",
  templateUrl: "./origens.component.html",
  styleUrls: ["./origens.component.scss"]
})
export class OrigensComponent implements OnInit {

  currentEmp: boolean;
  listOrigens = [];
  loader = true;

  constructor(
    public api: ApiService,
    public global: GlobalService
  ) {
    // se nao existir empresa selecionada ( empresaSelect.id = 0), o componente fica observando a variavel no service API
    // para saber que quando a empresa foi selecionada, pode buscar as origens e criar os objetos
        // andre descomentando.
       if(api.objCtrl.empresaSelect.id == 0){
        //alert("origens.componment.ts:: construtor")
        // andre comentando
          api.empresa$.subscribe((newBool: boolean) => {
            this.currentEmp = newBool;
            if(this.currentEmp){
              this.generateOrigens();
            }
          });

       }else{
         // se já tem empresa selecionada, apenas busca as origens e cria os objetos
         //alert("origens.componment.ts:: construtor else empresa selecionada")
         this.generateOrigens();
       }

  }

  ngOnInit() {

  }


  generateOrigens(){

    //alert("origens.component.ts :: generateOrigens() - codigo empresa:" + this.api.objCtrl.empresaSelect.id);

    this.api.getOrigens({
      NomeUser: this.api.objCtrl.UserSenior,
      CodEmp: this.api.objCtrl.empresaSelect.id,
      Tenant: this.api.objCtrl.Tenant
    }).then((res:any) => {

     // alert("origens.component.ts::voltou do getOrigens.")
      this.loader = true;
      
      this.listOrigens = res.map(function(item, index) {
          let linkRouter = "menuop/";
          return {
            id:         item.id,
            codOrigem:  item.codigoOrigem,
            name:       item.descricaoOrigem,
            route:      linkRouter+item.tipoApontamento+'/'+item.codigoOrigem
          };
      });
    })
    .catch(err => {
      //alert(err);
      this.loader = false;
      this.listOrigens = [];
      this.global.showToast({type:'error',title:'Ocorreu um erro',text:'Erro ao buscar origens, tente novamente.'});
    });
  }



  showVariables(){
    console.group('Variables');
      // console.log('objCtrl: ', this.objCtrl, '-------------------');
      // console.log('empselected: ', this.empselected);
      //console.log('listOrigens: ', this.listOrigens);
    console.groupEnd();
  }

}