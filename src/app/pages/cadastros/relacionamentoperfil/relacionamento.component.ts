import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { GlobalService } from '../../../services/global/global.service';
import { ApiService } from '../../../services/api/api.service';

declare var $: any;

@Component({
  selector: 'app-relacionamento',
  templateUrl: './relacionamento.component.html',
  styleUrls: ['./relacionamento.component.scss']
})
export class RelacionamentoComponent implements OnInit {
  currentEmp: boolean;

  objCtrl = {
    model:{
      usuario: 0,
      perfis: []
    },

    //vindo da API
    usuario:[],
    perfis:[]

  }

  //objeto para envio à API
  usuarioApi = {
    idUsuario: 0,
    idsPerfil: []
  }

  constructor(
    public api: ApiService,
    public global: GlobalService
  ) { 
    // se nao existir empresa selecionada ( empresaSelect.id = 0), o componente fica observando a variavel no service API
    // para saber que quando a empresa foi selecionada, pode buscar os perfis e criar os objetos
    if(api.objCtrl.empresaSelect.id == 0){
      api.empresa$.subscribe((newBool: boolean) => {
        this.currentEmp = newBool;
        if(this.currentEmp){
          this.generateList();
        }
      });
    }else{
      // se já tem empresa selecionada, apenas busca os perfis e cria os objetos
      this.generateList();
    }
  }

  ngOnInit() {
  }

  generateList(){

    this.objCtrl.usuario = []
    this.objCtrl.model.usuario = 0

    this.createListUsuario().then(() => {
      this.createListPerfis().then(() => {

        // this.objCtrl.usuario.map((item,index)=>{
        //   this.objCtrl.perfis.map(ori => {
        //     item.perfis.push(Object.assign({},ori));
        //   });
        // });

      })
    })
    
    // console.log(this.objCtrl.usuario);
  }

  createListUsuario(){

    return new Promise((resolve,reject) => {

      //Retorna lista de usuários cadastrados, por empresa
      this.api.getUsuario(this.api.objCtrl.empresaSelect.id,this.api.objCtrl.Tenant)
      .then((res) => {

        this.objCtrl.usuario = (    
          res.usuarios.map((item) => { 
            return {
              id: item.id,
              name: item.nomeUsuario,
              perfis: []
            }
          })
        )
        resolve()
      })
      .catch((err) => {
        reject()
      })


    }) 

  }

  createListPerfis(){

    return new Promise((resolve,reject) => {

      this.objCtrl.usuario.map((item) => {

        //Busca os perfis alocados ao usuário retornado  
        this.api.getPerfisUsuario(
          this.api.objCtrl.empresaSelect.id, 
          item.id, 
          this.api.objCtrl.Tenant
        ).then((resPerfis) => {
          
          this.objCtrl.perfis = []
  
          // this.objCtrl.perfis = (
          item.perfis = (
            resPerfis.perfis.map((itemPerfis) => {      
              
              let status = false
  
              if(itemPerfis.idUsuario == item.id) {
                status = true
              }                 
              return {
                id: itemPerfis.idPerfil,
                name: itemPerfis.nomePerfil,
                ativo: status
              }
            })
          )

          resolve()

        }).catch((errPerfis => {
          console.log(errPerfis)
          reject()
        }))
  
      })

    })

  
  }

  changeusuario(event){
    this.objCtrl.usuario.map((item)=>{
      if(item.id == event){
        this.objCtrl.model.usuario = item.id;
        this.objCtrl.model.perfis = item.perfis;
      }
    });
  }

  salvar(){
    this.global.showConfirm({header:'Salvar registros', message:'Deseja salvar os registros alterados?', icon:'fas fa-clipboard-check text-default'}).then(res =>{
      if( res == true ){
 
        //Prepara o objeto para envio à API
        this.usuarioApi.idUsuario = this.objCtrl.model.usuario
        this.usuarioApi.idsPerfil = []

        this.objCtrl.model.perfis.map((res) => {

          if(res.ativo == true) {
            this.usuarioApi.idsPerfil.push(res.id)
          }

        })

        //Realiza a chamada da API
        this.api.generateAssociacaoPerfis(
          this.api.objCtrl.empresaSelect.id,
          this.api.objCtrl.Tenant,
          this.usuarioApi
        ).then((res) => {
          
          this.global.showToast({
            type:'success',
            title:'Sucesso!',
            text:'Perfis salvos!'
          });
          this.generateList();

        })
        .catch((err) => {

          this.global.showToast({
            type:'error',
            title:'Ocorreu um erro',
            text: err.mensagem,
            position:'center',
            life:12000
          });

        })
        
      }
    });
  }

  cancelar(){
    this.global.showConfirm({header:'Cancelar registros?', message:'Deseja cancelar os registros alterados?', icon:'fas fa-trash text-red'}).then(res =>{
      if( res == true ){
        this.global.showToast({
          type:'success',
          title:'Sucesso!',
          text:'Alteração nos Usuários foram cancelados!'
        });
        this.generateList();
      }
    });
  }


}