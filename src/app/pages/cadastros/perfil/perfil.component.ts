import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../services/global/global.service';
import { ApiService } from '../../../services/api/api.service';

import { SelecionaEmpresaComponent } from '../../seleciona-empresa/seleciona-empresa.component';

declare var $: any;

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  currentEmp: boolean;

  objCtrl = {
    model:{
      perfil: 0,
      origens: []
    },

    //vindo da API
    perfil:[],
    origens:[],
    origensOriginal:[],

    listTipo:[
      {id: 1, name: 'Agrupada'},
      {id: 2, name: 'Individual'}
    ]

  }

  //objeto para envio à API
  listaOrigens = {
    idPerfil: 0,
    origens: []
  }

  constructor(
    public api: ApiService,
    public global: GlobalService
  ) { 
    // se nao existir empresa selecionada ( empresaSelect.id = 0), o componente fica observando a variavel no service API
    // para saber que quando a empresa foi selecionada, pode buscar os perfis e criar os objetos
    if(api.objCtrl.empresaSelect.id == 0){
      api.empresa$.subscribe((newBool: boolean) => {
        this.currentEmp = newBool;
        if(this.currentEmp){
          this.generateList();
        }
      });
    }else{
      // se já tem empresa selecionada, apenas busca os perfis e cria os objetos
      this.generateList();
    }
  }

  ngOnInit() {
  }

  generateList(){

    this.objCtrl.perfil = [];
    this.objCtrl.model.perfil = 0;

    
    this.api.getPerfil(this.api.objCtrl.empresaSelect.id,this.api.objCtrl.Tenant)
      .then((res) => {

        this.objCtrl.perfil = (
            
          res.perfis.map((item) => {
            return {
              id: item.id,
              name: item.nomePerfil,
              ativo: false,
              origens: item.origens.map((itemOri) => {
                return {
                  id: itemOri.id, 
                  codorigem: itemOri.codigoOrigem,
                  name: itemOri.descricaoOrigem, 
                  tipo: itemOri.tipoApontamento, 
                  ativo: true
                }
              })
            }
          })

        )

        //Retorna todas as OP's na empresa através da API
        this.api.getOrigensGeral({
          NomeUser: this.api.objCtrl.UserSenior,
          CodEmp: this.api.objCtrl.empresaSelect.id,
          Tenant: this.api.objCtrl.Tenant
        }).then((resGeral:any) => {

          this.objCtrl.origensOriginal = resGeral.lista.map((item) => {
              return {
                id: item.id,
                codOrigem: item.codigoOrigem,
                name: item.descricaoOrigem,
                tipo: 0,
                ativo: false
              };
          });
 
          //Varre os perfis retornados
          this.objCtrl.perfil.map((item,index)=>{
            
           
            //A cada perfil, atualizo a lista de origens com as originais 
            this.objCtrl.origens = []
            this.objCtrl.origens = this.objCtrl.origensOriginal.map((listaOriginal) => {
              return {
                id: listaOriginal.id,
                codOrigem: listaOriginal.codOrigem,
                name: listaOriginal.name,
                tipo: listaOriginal.tipo,
                ativo: listaOriginal.ativo
              };
            })

            //Varre as origens ligadas ao perfil retornado
            item.origens.map((itemOri) => {

              //Para cada origem carregada no getOrigemGeral, retirará as origens que ja estão no perfil
              this.objCtrl.origens.map((ori,indexOri)=>{
              
                if(ori.id == itemOri.id) {
                  this.objCtrl.origens.splice(indexOri,1)
                }

              });  

            })

            this.objCtrl.origens.map((itemOri,index) => {
              item.origens.push(itemOri)
            })

            //Ordeno os valores do objeto
            item.origens.sort(function(a,b){return a.id - b.id})

          });

        })
        .catch(err => {
          console.log(err)
        });


      })
      // .then(() => {

      //   this.api.getOrigensGeral({
      //     NomeUser: this.api.objCtrl.UserSenior,
      //     CodEmp: this.api.objCtrl.empresaSelect.id,
      //     Tenant: this.api.objCtrl.Tenant
      //   }).then((res:any) => {
 
      //     this.objCtrl.origensOriginal = res.lista.map((item) => {
      //         return {
      //           id: item.id,
      //           codOrigem: item.codigoOrigem,
      //           name: item.descricaoOrigem,
      //           tipo: 0,
      //           ativo: false
      //         };
      //     });

      //     //Varre os perfis retornados
      //     this.objCtrl.perfil.map((item,index)=>{
            
      //       //a cada perfil, atualizo a lista de origens com as originais
      //       this.objCtrl.origens = this.objCtrl.origensOriginal

      //       //Varre as origens ligadas ao perfil retornado
      //       item.origens.map((itemOri) => {

      //         //para cada origem carregada no getOrigemGeral, retirará as origens que ja estão no perfil
      //         this.objCtrl.origens.map((ori,indexOri)=>{
               
      //           if(ori.id == itemOri.id) {
      //             this.objCtrl.origens.splice(indexOri,1)
      //           }

      //         });  

      //       })

      //       this.objCtrl.origens.map((itemOri,index) => {
      //         item.origens.push(itemOri)
      //       })

      //       item.origens.sort(function(a,b){return a.id - b.id})

      //     });

      //   })
      //   .catch(err => {
      //     console.log(err)
      //   });

      // })
      .catch((err) => {
        console.log(err)
      })

    // this.objCtrl.perfil = [];
    // this.objCtrl.model.perfil = 0;
    // this.createList();

    // this.objCtrl.perfil.map((item,index)=>{
    //   this.objCtrl.origens.map(ori=>{
    //     item.origens.push(Object.assign(this.objCtrl.origens,ori));
    //   });
    // });
    
  }

  createList(){
    this.objCtrl.perfil.push(
      {id: 1, name: 'Master', ativo: false, origens: []},
      {id: 2, name: 'Cadastrador', ativo: false, origens: []},
      {id: 3, name: 'Financeiro', ativo: false, origens: []},
      {id: 4, name: 'Comercial', ativo: false, origens: []},
      {id: 5, name: 'Desenvolvedor', ativo: false, origens: []},
      {id: 6, name: 'Analista', ativo: false, origens: []}
    );
  }

  changeperfil(event){
    this.objCtrl.perfil.map((item)=>{
      if(item.id == event){
        this.objCtrl.model.perfil = item.id;
        this.objCtrl.model.origens = item.origens;
      }
    });
  }

  salvar(){
    
    let hasError = false;
    
    // this.objCtrl.model.origens.filter(item => {
    //   if(item.ativo == true && item.tipo == 0){
    //     hasError = true;
    //     this.global.showToast({type:'warn',title:'Atenção', text:`Selecione um tipo para o item: ${item.id} - ${item.name}`});
    //   }
    // });

    if(!hasError){
      this.global.showConfirm({header:'Salvar registros', message:'Deseja salvar os registros alterados?', icon:'fas fa-clipboard-check text-default'}).then(res =>{
        if( res == true ){

          console.log(this.objCtrl.model.origens)

          this.listaOrigens.origens = []

          this.listaOrigens.idPerfil = this.objCtrl.model.perfil
          this.objCtrl.model.origens.map((item) => {
            if(item.ativo == true) {
              this.listaOrigens.origens.push({
                idOrigem: item.id
              })
            }
          })

          this.api.generateAssociacaoOrigens(
            this.api.objCtrl.empresaSelect.id,
            this.api.objCtrl.Tenant,
            this.listaOrigens
          ).then((res) => {
            console.log(res)

            this.global.showToast({
              type:'success',
              title:'Sucesso!',
              text:'Perfis Salvos!'
            });
            this.generateList();

          }).catch((err) => {
            console.log(err)

            this.global.showToast({
              type:'error',
              title:'Ocorreu um erro',
              text:'Tente novamente ou entre em contato com o suporte técnico.',
              position:'center',
              life:12000
            });

          })


          console.log(this.listaOrigens)

          // this.global.showToast({
          //   type:'success',
          //   title:'Sucesso!',
          //   text:'Perfis Salvos!'
          // });
          // this.generateList();
        }
      });
    }
  }

  cancelar(){
    this.global.showConfirm({header:'Cancelar registros?', message:'Deseja cancelar os registros alterados?', icon:'fas fa-trash text-red'}).then(res =>{
      if( res == true ){
        this.global.showToast({
          type:'success',
          title:'Sucesso!',
          text:'Alteração nos Perfis foram cancelados!'
        });
        this.generateList();
      }
    });
  }


}