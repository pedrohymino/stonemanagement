import { Component, OnInit, Injectable } from '@angular/core';
import { GlobalService } from "../../services/global/global.service";
import { ApiService, empresa } from "../../services/api/api.service";

declare var $: any;

@Component({
  selector: 'app-seleciona-empresa',
  templateUrl: './seleciona-empresa.component.html',
  styleUrls: ['./seleciona-empresa.component.scss']
})
export class SelecionaEmpresaComponent implements OnInit {
  empselected;
  objCtrl = {
    lastEmp: 0,
    preSelect:0
  };

  constructor(
    public api?: ApiService,
    public global?: GlobalService
  ) {
    this.empselected = this.api.getCurrentEmpresa().id;
  }

  ngOnInit() {
    if(this.api.getCurrentEmpresa().id == 0){ // 0 = nao existe empresa
      this.escolherEmpresa();
    }
  }

  escolherEmpresa(){
    this.empselected = this.api.getCurrentEmpresa().id;
    $("#chooseEmpresa").modal({
      show: true,
      backdrop: 'static'
    });
  }

  confirmEmpresa(id){
    this.objCtrl.lastEmp = id;
    this.api.changeEmpresa(id);
  }

  cancelEmpresa(){
    // removido essa linha, pois bugava: EXEMPLO
    // 1- Entrar na página origens e escolher qlqr empresa
    // 2- mudar para pagina cadastro perfil
    // 3- Abrir modal para mudar de empresa e clicar em cancelar
    // ai ele perdia a referencia da empresa que estava selecionada
    // this.empselected = this.objCtrl.lastEmp; 
    this.objCtrl.preSelect = 0;
  }

  preselectEmpresa(idPre){
    this.objCtrl.preSelect = idPre;
  }


  /*
  ERROS
  502 - ERP fora do ar
  404 - Cliente não encontrado
  */
}
