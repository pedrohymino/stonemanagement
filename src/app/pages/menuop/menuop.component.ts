import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../services/api/api.service";
import { GlobalService } from "../../services/global/global.service";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-menuop",
  templateUrl: "./menuop.component.html",
  styleUrls: ["./menuop.component.scss"]
})
export class MenuopComponent implements OnInit {
  idParam;
  CodOriParam;
  listOps = [];
  loader = true;
    constructor(
      private route: ActivatedRoute,
      public api: ApiService,
      public global: GlobalService,
      private router: Router
    ) {
    this.idParam = this.route.snapshot.paramMap.get("id");
    this.CodOriParam = this.route.snapshot.paramMap.get("codorigem");
  }

  ngOnInit() {
    let routerLink='';

    if(this.idParam == 1){ // origem que 
      routerLink = '/consultalote/';
    }else{
      routerLink = '/apotamentoindividual/';
    }

    this.api.getOps({
      User: this.api.objCtrl.User,
      Senha: this.api.objCtrl.Senha,
      UserSenior: this.api.objCtrl.UserSenior,
      CodOri: this.CodOriParam,
      CodEmp: this.api.objCtrl.empresaSelect.id,
      Tenant: this.api.objCtrl.Tenant
    }).then((res:any) => {
      if(res == null){
        this.global.showConfirm({
          header:'Atenção',
          message:`Não existe OP's na Origem(${this.CodOriParam}), deseja voltar para a página anterior?`,
          icon: 'fas fa-bell text-_warn'
        }).then(res =>{
          if( res == true ){
            this.router.navigate(['origens']);
          }else{
            this.loader = false;
          }
        });
        
      }else{
        
        //console.log('menuOP: ',res)

        this.listOps = res.map((item, index) => {
          
          let percent;

          if(item.qtdApontamentosRealizados > 0){
            percent = (item.qtdApontamentosRealizados/item.totalLotesOp)*100;
          }else{
            percent = 0;
          }
          return {
            id: item.numeroOp,
            origem: this.CodOriParam,
            status: item.situacaoOp,
            statusApp: item.situacaoOpAplicativo,
            date: item.dataGeracao,
            name: item.descricaoProduto,
            restante: item.qtdLotesSemApontamento,
            feito: item.qtdApontamentosRealizados,
            total: item.totalLotesOp,
            percent,
            index,
            router: routerLink+this.CodOriParam+'/'+item.numeroOp+'/'+item.codigoProduto+'/'+this.idParam
            //router: routerLink+item.numeroOp+'/'+item.codigoProduto+'/'+this.idParam
          };
        });
      }
    })
    .catch((err:any) => {
      this.global.showToast({
        type:'error',
        title:'Ocorreu um erro',
        text:'Tente novamente ou entre em contato com o suporte técnico.',
        position:'center',
        life:12000
      });
      this.loader = false;
    });
    
  }

  selecionaOp(index) {
    this.global.registraOp(this.listOps[index])
    this.router.navigate([this.listOps[index].router]);
  }

}
