import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "../../services/global/global.service";
import { ApiService } from "../../services/api/api.service";

declare var $: any;

@Component({
  selector: 'app-apontamentoindividual',
  templateUrl: './apontamentoindividual.component.html',
  styleUrls: ['./apontamentoindividual.component.scss']
})
export class ApontamentoindividualComponent implements OnInit {
  idParam;
  CodProd;
  CodOri;
  lastpage;
  loader = true;
  
  esperarApontamento = false

  objCtrl = {
    listLote:[],
    listNaoApontado: [],
    currentLote: 0,
    currentLoteObj:null,
    listTipo: [],

    // quadrante
    viewQuadrante: [],

    // defeito
    boxDefeito: false,
    currentApontamento: [],
    currentQuadrante: [],

    // qualidade
    currentQualidade: [],

    autofill: {
      comprimento: 1,
      largura: 1.20,
      tipo: 2
    },
    quadranteSelect:null,
    model: {
      op: '',
      bloco: '',
      largura: null,
      comprimento: null,
      tipo: 0,
      codigoComponente: '',
      derivacaoComponente: ''
    }
  }

  //objeto para apontamento dos lotes
  apontamentoLotes = {
    codigoEmpresa: 0,
    codigoOrigem:'',
    numeroOP: 0,
    codigoComponente: '',
    derivacaoComponente: '',
    tipoApontamento: 2,
    tipoDespositivo: 0,
    usuarioSeniorX: '',
    lotes: [{
      codigoQualidade: '',
      codigoLote: '',
      dataFinal: null,
      horaFinal: 0,
      dimensaoComprimento: 0,
      dimensaoLargura: 0,
      defeitos: [{
        sequenciaDefeito: 0,
        //codigoDefeito: 0,
        quadranteDefeito: ''
      }]
    }]
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public global: GlobalService,
    public api: ApiService
  ) {
    this.idParam = this.route.snapshot.paramMap.get("id");
    this.CodProd = this.route.snapshot.paramMap.get("prod");
    this.CodOri = this.route.snapshot.paramMap.get("codori");
    this.lastpage = this.route.snapshot.paramMap.get("lastpage");

    this.global.showToast({
      type:'info',
      title:'Importante',
      text:'Para apontar, é necessário escolher o lote desejado!',
      life:5000
    });
  }


  ngOnInit() {

    this.api.getLote({
      User: this.api.objCtrl.User,
      Senha: this.api.objCtrl.Senha,
      UserSenior: this.api.objCtrl.UserSenior,
      NumOP: this.idParam,
      CodEmp: this.api.objCtrl.empresaSelect.id,
      CodOri: this.CodOri,
      CodProd: this.CodProd,
      Tenant: this.api.objCtrl.Tenant
    }).then((res:any) => {
      if(res == null){
        this.global.showConfirm({
          header:'Atenção',
          message:`Não existe OP's na Origem(${this.CodProd}), deseja voltar para a página anterior?`,
          icon: 'fas fa-bell text-_warn'
        }).then(res =>{
          if( res == true ){
            this.router.navigate([`menuop/${this.lastpage}/${this.CodOri}`]);
          }else{
            this.loader = false;
          }
        });
        
      }else{
        this.objCtrl.model.bloco = res.codigoBloco;
        this.objCtrl.model.op = this.idParam;

        this.objCtrl.model.codigoComponente = res.codigoComponente
        this.objCtrl.model.derivacaoComponente = res.derivacaoComponente

        res.listarLotes.map((item) => {
          let status; // 1 => nao paontado | 2 => apontado Perfeito | 3 => apontado Defeito
          if(item.apontamentoRealizado == null && item.apontamentoDefeito == null){
            status = 1; 
          }else if((item.apontamentoRealizado).toLowerCase() == 'ok'){
            status = 2;
          }else if((item.apontamentoDefeito).toLowerCase() == 'ok'){
            status = 3;
          }
          this.objCtrl.listLote.push({
            id: parseInt((item.codigoLote).split('-')[1]),
            name: item.codigoLote,
            comprimento: item.valorDimensao1,
            largura: item.valorDimensao2,
            qualidade: item.codigoQualidade,
            status
          })
        });

        for(var i=1; i<5; i++){
          this.objCtrl.listTipo.push({id:i, name: 'opção'})
        }
  
        this.initCurrentApontamento();
        this.initViewQuadrante();
        this.createListSelect();

      }
    }).catch((err:any) => {
      console.log('erroTela',err);
      this.global.showToast({
        type:'error',
        title:'Ocorreu um erro',
        text:'Tente novamente ou entre em contato com o suporte técnico.',
        position:'center',
        life:12000
      });
      this.loader = false;
    });


    // for(var i=1; i<57; i++){
    //   //                                 É a quantidade max de números
    //   //                                 v 
    //   let id = i;
    //   this.objCtrl.listLote.push({
    //     id,
    //     name: this.objCtrl.model.bloco+'-'+this.global.addZeroBefore(3,id),
    //     status: this.global.randomNum(1,3) // 1 => nao paontado | 2 => apontado Perfeito | 3 => apontado Defeito
    //   })
    // }

  }

  
  // executa depois que toda a página foi renderizada
  ngAfterViewInit(){
    this.boxResizable();
    
  }
  
  
  boxResizable(){
    // habilita resizable na classe viewlist (n,s) => n=norte, s= sul
    $(".viewlist").resizable({
      handles: 'n, s',
      maxHeight: $('.viewlist .mainlist').height()+10
    });
  }
  

  initCurrentApontamento(){
    
    this.objCtrl.currentApontamento = [];

    //Busca e Preenchemento de Defeitos
    this.api.generateDefeitos({
      CodEmp: this.api.objCtrl.empresaSelect.id,
      CodOri: this.CodOri,
      UserSenior:this.api.objCtrl.UserSenior,
      Tenant: this.api.objCtrl.Tenant
    }).then(() => {

      this.api.getDefeitos().then((res:any) => {

        for(var i=1; i<=9; i++){
          this.objCtrl.currentApontamento.push({
            nQuadrante:i,
            defeitos: [] = res.map(item=>{
              return {
                id:item.codigoDefeito,
                name:item.descricaoDefeito,
                check:false
              }
            })
          });
        }

      });

    });

    //Busca e Preenchimento de Qualidades
    this.api.generateQualidade({
      CodEmp: this.api.objCtrl.empresaSelect.id,
      CodOri: this.CodOri,
      UserSenior:this.api.objCtrl.UserSenior,
      Tenant: this.api.objCtrl.Tenant
    }).then((res: any) => {

      (res.map(item => {
        this.objCtrl.currentQualidade.push(item)  
      }))

    })

  }

  initViewQuadrante(){
    this.objCtrl.viewQuadrante = [];
    for(var i=1; i<=9; i++){
      this.objCtrl.viewQuadrante.push({
        num:i,
        qtdDefeito: 0
      });
    }
  }

  alertSuccessApontamento(){
    this.global.showToast({
      type:'success',
      title:'Sucesso!',
      text:`Lote: ${this.global.addZeroBefore(3,this.objCtrl.currentLoteObj.id)} apontado com sucesso.`
    });
  }

  createListSelect(){
    // cria lista com os itens que podem ser selecionados (apenas itens que ainda nao foram apontados [status ==1] )
    this.objCtrl.listLote.filter(item=>{

      if(item.status == 1){
        let ax = {
          id: item.id,
          name: item.name,
          nameSelect: item.nameSelect,
          status: item.status,
          comprimento: item.comprimento,
          largura: item.largura,
          qualidade: item.qualidade,
          defeitos:[]
        }
        this.objCtrl.listNaoApontado.push(ax);
        // console.log('CRAIADO',this.objCtrl.listNaoApontado)
      }
    });
  }



  changeCurrentLote(event){
    this.objCtrl.currentLote = event;
    this.setCurrentLoteObj(this.objCtrl.currentLote);
  }

  // Button Prev <
  prevLote(){
    let aux=0;
    if(this.objCtrl.listNaoApontado.length>0){
      this.objCtrl.listNaoApontado.map((item,index)=>{
        if(item.id == this.objCtrl.currentLote){
          aux = index
        }
      });
      if(aux > 0){
        this.objCtrl.currentLote = this.objCtrl.listNaoApontado[aux-1].id;
      }else{
        this.objCtrl.currentLote = this.objCtrl.listNaoApontado[aux].id;
      }
      this.setCurrentLoteObj(this.objCtrl.currentLote);
    }else{
      this.semApontamento();
    }
  }

  // Button Next >
  nextLote(){
    let aux = this.objCtrl.listNaoApontado.length-1;
    if(this.objCtrl.listNaoApontado.length>0){
      this.objCtrl.listNaoApontado.map((item,index)=>{
        if(item.id == this.objCtrl.currentLote){
          aux = index
        }
      });
      if(aux < this.objCtrl.listNaoApontado.length-1){
        this.objCtrl.currentLote = this.objCtrl.listNaoApontado[aux+1].id;
      }else{
        this.objCtrl.currentLote = this.objCtrl.listNaoApontado[aux].id;
      }
      this.setCurrentLoteObj(this.objCtrl.currentLote);
    }else{
      this.semApontamento();
    }
  }

  setCurrentLoteObj(id){
    this.objCtrl.listNaoApontado.map((item,index)=>{
      if(item.id == id){   
        this.objCtrl.currentLoteObj = item;
        //this.objCtrl.currentLoteObj.comprimento = this.objCtrl.autofill.comprimento;
        //this.objCtrl.currentLoteObj.largura = this.objCtrl.autofill.largura;
        //this.objCtrl.currentLoteObj.qualidade = this.objCtrl.autofill.tipo;
      }
    });
    // setTimeout(() => {
    //   this.boxResizable();
    // }, 500);
  }



  apontar(){

    this.global.showConfirm({
      header: 'Confirmar apontamento',
      message: 'Deseja concluir o apontamento?',
      icon: 'fas fa-clipboard-check text-default'
    }).then((res) => {

      if( res == true ){

        this.checkFieldsApontamento().then(res => {
          
          this.enviaApontamentoLotes()
            .then(() => {
              
              this.objCtrl.listNaoApontado.map((item,index) => {
                if(item.id == this.objCtrl.currentLoteObj.id){
                  this.objCtrl.listNaoApontado.splice(index, 1);
                }
              });
              this.objCtrl.listLote.map((item,index) => {
                if(item.id == this.objCtrl.currentLoteObj.id){
                  item.status=2;
                }
              });

              this.alertSuccessApontamento(); // mostra toast com apontamento successo do bloco X
              this.prevLote();
              //this.clearAllVariables()
  
            })

          //this.alertSuccessApontamento(); // mostra toast com apontamento successo do bloco X
          //this.prevLote();
        }).catch(err => {
    
        });
  
      }

    })

  }

  defeito(){
    this.checkFieldsApontamento().then(res => {
      this.objCtrl.boxDefeito = true;
      this.objCtrl.currentLoteObj.status = 3;
    }).catch(err => {

    });
  }



  checkFieldsApontamento(){
    return new Promise((resolve, reject)=>{
      if(this.objCtrl.currentLoteObj.comprimento == null || this.objCtrl.currentLoteObj.comprimento == 0){
        this.global.showToast({
          type:'error',
          title:'Campo não preenchido!',
          text:'Campo Comprimento não foi preenchido!'
        });
        reject();
      }else if(this.objCtrl.currentLoteObj.largura == null || this.objCtrl.currentLoteObj.largura == 0){
        this.global.showToast({
          type:'error',
          title:'Campo não preenchido!',
          text:'Campo Largura não foi preenchido!'
        });
        reject();
      }else if(this.objCtrl.currentLoteObj.tipo == 0){
        this.global.showToast({
          type:'error',
          title:'Campo não preenchido!',
          text:'Campo Tipo não foi preenchido!'
        });
        reject();
      }else{
        resolve();
      }
    });
  }


  addQuadranteDefeito(num){
    this.objCtrl.quadranteSelect = num;
    this.objCtrl.currentQuadrante = this.objCtrl.currentApontamento[num-1];
  }



  checkDefeito(id){
    this.objCtrl.currentApontamento.filter((item,index)=>{
      if(item.nQuadrante == this.objCtrl.quadranteSelect){   
        let aux = item.defeitos;
        aux.map((itemdef,indexdef)=>{
          if(itemdef.id == id){
            itemdef.check = !itemdef.check;
          }
        });
        this.objCtrl.viewQuadrante.map(vqdra=>{
          if(vqdra.num == this.objCtrl.quadranteSelect){
            vqdra.qtdDefeito=0;
            aux.filter(axt=>{
              if(axt.check){
                vqdra.qtdDefeito++;
              }
            })
          }
        });

        item.defeitos = aux;
        aux = [];
      }
    });
  }

  delAllDefeitos(){
    this.objCtrl.currentApontamento.filter((item,index)=>{
      if(item.nQuadrante == this.objCtrl.quadranteSelect){   
        let aux = item.defeitos;
        aux.map((itemdef,indexdef)=>{
          itemdef.check = false;
        });
        this.objCtrl.viewQuadrante.map(vqdra=>{
          if(vqdra.num == this.objCtrl.quadranteSelect){
            vqdra.qtdDefeito=0;
          }
        });

        item.defeitos = aux;
        aux = [];
      }
    });
  }



  cancelApontamento(){
    this.global.showConfirm({
      header: 'Confirmar exclusão',
      message: 'Deseja cancelar o apontamento?',
      icon: 'fas fa-trash text-red'
    }).then(res =>{
      if( res == true ){
        this.clearAllVariables();
        this.global.showToast({
          type:'info',
          title:'Informação',
          text:'Seleções de defeito cancelados!',
          position:'center'
        });
      }
    });
  }
  
  registraApontamento(){

    this.global.showConfirm({
      header: 'Confirmar apontamento',
      message: 'Deseja concluir o apontamento?',
      icon: 'fas fa-clipboard-check text-default'
    }).then(res =>{
      if( res == true ){
        //EXEMPLO => defeito:[ {quadrante: 1,defeitos: [1,4,5]} , {quadrante: 3,def: [8]} ]
        let defeitosAux = []
        this.objCtrl.currentApontamento.filter(item=>{
          let numax = item.nQuadrante;
          let arrDf = [];
          item.defeitos.map(it=>{
            if(it.check){
              arrDf.push(it.id);
            }
          });
          if(arrDf.length > 0){
            defeitosAux.push({quadrante: numax, defeitos: arrDf});
          }
        });
        this.checkFieldsApontamento().then(res => {
          
          if(defeitosAux.length > 0){
        
            this.objCtrl.currentLoteObj.defeitos = defeitosAux;
            
           //console.log(this.objCtrl.currentLoteObj);

            // Chama metodo para perparacao e envio de dados ao ERP - Enviar para o ERP via API o => this.objCtrl.currentLoteObj
            this.enviaApontamentoLotes()
              .then(() => {
                
                this.objCtrl.listNaoApontado.map((a,ii) => {
                  if(a.id == this.objCtrl.currentLoteObj.id){
                    this.objCtrl.listNaoApontado.splice(ii, 1);
                  }
                });
                this.objCtrl.listLote.map((b) => {
                  if(b.id == this.objCtrl.currentLoteObj.id){
                    b.status=3;
                  }
                });

                this.alertSuccessApontamento(); // mostra toast com apontamento successo do bloco X
                this.prevLote();
                this.clearAllVariables()
    
              })

          }else{
            this.global.showToast({
              type:'error',
              title:'Nenhum defeito selecionado!',
              text:'Para apontar um defeito é necessário selecionar ao menos 1 defeito do lote!'
            });
          }
        }).catch(err => {
    
        })
        
      }
    });

  }

  enviaApontamentoLotes() {
  
    return new Promise((resolve,reject)=>{

      this.apontamentoLotes.lotes = []

      this.apontamentoLotes.codigoEmpresa = this.api.objCtrl.empresaSelect.id
      this.apontamentoLotes.codigoOrigem = this.CodOri
      this.apontamentoLotes.numeroOP = this.idParam
      this.apontamentoLotes.codigoComponente = this.objCtrl.model.codigoComponente
      this.apontamentoLotes.derivacaoComponente = this.objCtrl.model.derivacaoComponente
      this.apontamentoLotes.tipoApontamento = 2
      this.apontamentoLotes.tipoDespositivo = 0
      this.apontamentoLotes.usuarioSeniorX = this.api.objCtrl.UserSenior
           
      //variavel para recurso de data e hora local
      let date = new Date()
      let hora = date.getMinutes() + (date.getHours() * 60)
      
      //array para acumular os defeitos inclusos no lote
      let listaDefeitos = []

      //separa os defeitos vinculados ao lote
      this.objCtrl.currentLoteObj.defeitos.map((item) => {
          
        item.defeitos.map((itemDef, indexDef) => {
          listaDefeitos.push({
            sequenciaDefeito: item.quadrante,
            quadranteDefeito: itemDef
          })
        })

      })

      //prepara objeto para apontamento
      this.apontamentoLotes.lotes.push({
        codigoQualidade: this.objCtrl.currentLoteObj.qualidade,
        codigoLote: this.objCtrl.currentLoteObj.name,
        dataFinal: this.global.getDate(date),
        horaFinal: hora,
        dimensaoComprimento: this.objCtrl.currentLoteObj.comprimento,
        dimensaoLargura: this.objCtrl.currentLoteObj.largura,
        defeitos: listaDefeitos.map((item) => {
          return {
            sequenciaDefeito: item.sequenciaDefeito,
            quadranteDefeito: item.quadranteDefeito
          }
        })

      })

      this.esperarApontamento = true

      //executa chamada do servico via API
      this.api.generateApontamentoLotes(
        this.api.objCtrl.empresaSelect.id,
        this.api.objCtrl.Tenant,
        this.apontamentoLotes
      ).then((res) => {
   
        this.esperarApontamento = false

        resolve()
      })
      .catch((err) => {

        this.esperarApontamento = false

        this.global.showToast({
          type:'error',
          title:'Ocorreu um erro',
          text: err.erroExecucao,
          position:'center',
          life:12000
        });

        reject()
      })

    })

  }


  clearAllVariables(){
    this.objCtrl.boxDefeito = false;
    this.objCtrl.currentLoteObj.status = 1;
    this.objCtrl.quadranteSelect = null;
    this.objCtrl.currentQuadrante = null;
    this.objCtrl.currentQualidade = [];
    this.initCurrentApontamento();
    this.initViewQuadrante();
  }


  semApontamento(){
    this.clearAllVariables();
    this.objCtrl.currentLote = 0;
    this.objCtrl.currentLoteObj.comprimento = null;
    this.objCtrl.currentLoteObj.largura = null;
    this.objCtrl.currentLoteObj.tipo = 0;
    this.global.showConfirm({
      header: 'Alerta',
      message: 'Não existem mais apontamento para ser realizado!\nDeseja voltar para a página: Menu Op\'s?',
      icon: 'fas fa-bell text-_warn'
    }).then(res =>{
      if( res == true ){
        this.router.navigate([`menuop/${this.lastpage}/${this.CodOri}`]);
      }
    });
  }


  

  showVariables(){
    console.group('Variables');
      console.log('objCtrl: ', this.objCtrl, '-------------------');
      console.log('boxDefeito: ', this.objCtrl.boxDefeito);
      console.log('currentApontamento: ', this.objCtrl.currentApontamento);
      console.log('currentLoteObj: ', this.objCtrl.currentLoteObj);
      console.log('quadranteSelect: ', this.objCtrl.quadranteSelect);
      console.log('currentQuadrante: ', this.objCtrl.currentQuadrante);
      console.log('viewQuadrante: ', this.objCtrl.viewQuadrante);
      console.log('listLote: ', this.objCtrl.listLote); // FEITO
      console.log('listNaoApontado: ', this.objCtrl.listNaoApontado);
    console.groupEnd();
  }

}
