import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "../../services/global/global.service";
import { ApiService } from "../../services/api/api.service";
import * as moment from 'moment';


declare var $: any;

@Component({
  selector: "app-consultalote",
  templateUrl: "./consultalote.component.html",
  styleUrls: ["./consultalote.component.scss"]
})

export class ConsultaloteComponent implements OnInit {
  idParam;
  CodProd;
  lastpage;
  CodOri;
  loader = true;
  ptBR; //local Calendar language

  //controla modo de dispositivo movel
  visaoMobile = false;

  //dados gerais da op selecionada
  opSelecionada = []

  //controla se o apontamento foi iniciado ou nao
  apontamentoIniciado = false;

  esperarApontamento = false

  objCtrl = {
    // controle dos filtros: lote de , lote até
    listLote: [],
    listLoteate: [],
    
    listSelected: [], // controle dos box: Perfeito e Defeito
    listOptDefeitos: [
      // {id: 1, desc: 'Defeito'},
      // {id: 2, desc: 'Trincado'},
      // {id: 3, desc: 'Mancha'},
      // {id: 4, desc: 'Buraco'}
    ],
    showSelect: false,
    camposApontamento: true,
    model: {
      op: 0,
      bloco: '',
      codigoComponente: '',
      derivacaoComponente: '',
      dtini: null,
      dtfim: null,
      lotede: 0,
      loteate: 0
    }
  };

  //objeto para registro do apontamento de Inicio/Fim da OP
  apontamentoInicioFim = {
    cadastroOperador: 0,
    codigoEmpresa: 0,
    codigoOrigem: '',
    numeroOP: 0,
    codigoComponente: '',
    derivacaoComponente: '',
    dataApontamento: null,
    horaApontamento: 0,
    tipoApontamento: 0,
    tipoDespositivo: 0,
    usuarioSeniorX: ''
  }

  //objeto para apontamento dos lotes
  apontamentoLotes = {
    codigoEmpresa: 0,
    codigoOrigem:'',
    numeroOP: 0,
    codigoComponente: '',
    derivacaoComponente: '',
    tipoApontamento: 1,
    tipoDespositivo: 0,
    usuarioSeniorX: '',
    lotes: []
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public global: GlobalService,
    public api: ApiService
  ) {
    this.ptBR = this.global.localeCalendar();
    this.idParam = this.route.snapshot.paramMap.get("id");
    this.CodProd = this.route.snapshot.paramMap.get("prod");
    this.CodOri = this.route.snapshot.paramMap.get("codori");
    //this.CodOri = this.CodProd.split('.')[0];
    this.lastpage = this.route.snapshot.paramMap.get("lastpage");

    this.objCtrl.model.op = this.idParam;
  }
  
  ngOnInit() {

    this.opSelecionada.push(this.global.exibeOpSelecionada())

    if(this.opSelecionada[0].statusApp === null || this.opSelecionada[0].statusApp === 'C' || this.opSelecionada[0].statusApp === 'N') {
      this.apontamentoIniciado = false
    } else {
      this.apontamentoIniciado = true
    }


    // Parametro para controle de visão mobile ou desktop
    if(screen.width < 1024 || screen.height < 768) {
      this.visaoMobile = true;
    }


    this.api.getLote({
      User: this.api.objCtrl.User,
      Senha: this.api.objCtrl.Senha,
      UserSenior: this.api.objCtrl.UserSenior,
      NumOP: this.idParam,
      CodEmp: this.api.objCtrl.empresaSelect.id,
      CodOri: this.CodOri,
      CodProd: this.CodProd,
      Tenant: this.api.objCtrl.Tenant
    }).then((res:any) => {
      if(res == null){
        this.global.showConfirm({
          header:'Atenção',
          message:`Não existe OP's na Origem(${this.CodProd}), deseja voltar para a página anterior?`,
          icon: 'fas fa-bell text-_warn'
        }).then(res =>{
          if( res == true ){
            this.router.navigate([`menuop/${this.lastpage}/${this.CodOri}`]);
          }else{
            this.loader = false;
          }
        });
        
      }else{

        this.objCtrl.model.bloco = res.codigoBloco
        this.objCtrl.model.codigoComponente = res.codigoComponente
        this.objCtrl.model.derivacaoComponente = res.derivacaoComponente

        res.listarLotes.map((item) => {
          let status; // 1 => nao paontado | 2 => apontado Perfeito | 3 => apontado Defeito
          if(item.apontamentoRealizado == null && item.apontamentoDefeito == null){
            status = 1; 
          }else if((item.apontamentoRealizado).toLowerCase() == 'ok'){
            status = 2;
          }else if((item.apontamentoDefeito).toLowerCase() == 'ok'){
            status = 3;
          }
          this.objCtrl.listLote.push({
            id: parseInt((item.codigoLote).split('-')[1]),
            name: item.codigoLote,
            status
          })
        });
   
        this.generateListDefeitos(this.CodOri);

      }
    }).catch((err:any) => {
      console.log('erroTela',err);
      this.global.showToast({
        type:'error',
        title:'Ocorreu um erro',
        text:'Tente novamente ou entre em contato com o suporte técnico.',
        position:'center',
        life:12000
      });
      this.loader = false;
    });
    
  }


  // executa depois que toda a página foi renderizada
  ngAfterViewInit(){
    // habilita resizable na classe viewlist (n,s) => n=norte, s= sul
    $(".viewlist").resizable({
      handles: 'n, s',
      maxHeight: $('.viewlist .mainlist').height()+10
    });
  }

  
  checkDtFim(event){
    if(moment(event) > moment(this.objCtrl.model.dtfim)){
      this.objCtrl.model.dtfim = null;
    }
  }

  changeloteate(event){
    // controla o select loteate, se o loteDe for maior que o loteAte, ele pede para ser selecionado novamente
    // pois nao pode ser selecionado por ex:  de 006 até 001, e sim: de 001 até 006
    this.objCtrl.listLoteate = this.objCtrl.listLote.filter((item, index) => {
      return item.id >= event;
    });
    if(event > this.objCtrl.model.loteate){
      this.objCtrl.model.loteate = 0;
    }
  }  
  



  generateListDefeitos(CodOriParam){
    this.api.generateDefeitos({
      CodEmp: this.api.objCtrl.empresaSelect.id,
      CodOri: CodOriParam,
      UserSenior: this.api.objCtrl.UserSenior,
      Tenant: this.api.objCtrl.Tenant
    }).then(res=>{
 
      this.api.getDefeitos().then((res:any) => {

        this.objCtrl.listOptDefeitos.push(
          res.map(item=>{
            return {
              id:item.codigoDefeito,
              name:item.descricaoDefeito,
              check:false
            }
          })
        );
        
      });

    })
    .catch(err=>{
      console.error('Error: ', err);
    });


  }





  apontar(){

    if (!this.visaoMobile) {
      this.verificaDateFilter().then( res =>{
        this.verificaRangeLotes().then( res2 =>{
          this.setListSelect('deate');
          this.verificaIntervaloApontamento();
        }).catch(err2=>{
          // Lotes nao informados
        });
      }).catch(err=>{
        // Datas nao informadas
      });
    } else {
      this.verificaApontamentoIniciado().then(res => {
        
        this.verificaRangeLotes().then( res2 =>{
          this.setListSelect('deate');
          this.verificaIntervaloApontamento();
        }).catch(err2=>{
          // Lotes nao informados
        }); 

      }).catch(err => {
        //apontamento nao iniciado
      })
        
    }

    
  }

  apontarTudo(){

    if(!this.visaoMobile) {
      this.verificaDateFilter().then( res =>{
        // so executa se o campo data inicio e fim forem informados
        this.setListSelect('tudo');
        this.verificaIntervaloApontamento();
      }).catch(err=>{
        // Datas nao informadas
      });
    } else {
      this.verificaApontamentoIniciado().then(res => {
        this.setListSelect('tudo');
        this.verificaIntervaloApontamento();
      }).catch(err => {
        //apontamento nao iniciado
      })
    }

  }


  setListSelect(tipoApontamento){
    this.objCtrl.listSelected = [];
    this.objCtrl.listLote.filter((item) => {
      // controla ifs dependendo do tipo do apeontamento que chama
      if(tipoApontamento == 'tudo'){
        if(item.status == 1){
          this.addListSelect(item);
        }
      }else{
        if(item.status == 1 && item.id >= this.objCtrl.model.lotede && item.id <= this.objCtrl.model.loteate){
          this.addListSelect(item);
        }
      }
    });
  }

  addListSelect(item){
    let op: listops = {
      id: item.id,
      name: item.name,
      perfect: true,
      defeito: null
    }
    this.objCtrl.listSelected.push(op);
  }

  removelist(itemselect){
    this.objCtrl.listSelected.map((item,index) => {
      if(item.id == itemselect.id){
        this.objCtrl.listSelected.splice(index, 1);
      }
    });
  }

  moveTo(to, item){
    if(to == 'defeito'){
      item.perfect = false;
    }

    if(to == 'perfeito'){
      item.perfect = true;
      item.defeito = null;
    }
  }

  setDefeito(itemselect,defeito){
    this.objCtrl.listSelected.filter((item,index) => {
      if(itemselect.id == item.id){
        return item.defeito = defeito;
      }
    });
  }

  cancelApontamento(){
    this.global.showConfirm({
      header: 'Confirmar exclusão',
      message: 'Deseja cancelar o apontamento?',
      icon: 'fas fa-trash text-red'
    }).then(res =>{
      if( res == true ){
        this.objCtrl.showSelect = false;
        this.objCtrl.listSelected = [];
      }
    });

  }


  registraApontamento(){
    
    this.verificaDefeitoNaoApontado().then(res=>{
      this.global.showConfirm({
        header: 'Confirmar apontamento',
        message: 'Deseja concluir o apontamento?',
        icon: 'fas fa-clipboard-check text-default'
      }).then(res =>{
        if( res == true ){
     
          if(!this.visaoMobile) {
            this.apontarInicioFim('inicio').then((res1) => {
              this.enviaApontamentoLotes().then((res2) => {
                this.apontarInicioFim('fim').then((res3) => {
                }).catch((err3) => { })
              }).catch((err2) => { })
            }).catch((err1) => { })
          } else {
            this.enviaApontamentoLotes().then((res) => { 
            }).catch((err) => { })
          }

          //Zero o vetor de lotes ao final, independente do resultado
          this.apontamentoLotes.lotes = [];
        }
      });

    }).catch(err=>{

    });


  }

  enviaApontamentoLotes() {
  
    return new Promise((resolve,reject)=>{

      this.esperarApontamento = true

      //variavel para recurso de data e hora local
      let date = new Date()

      //preenche dados gerais para apontamento
      this.apontamentoLotes.codigoEmpresa = this.api.objCtrl.empresaSelect.id
      this.apontamentoLotes.codigoOrigem = this.CodOri
      this.apontamentoLotes.numeroOP = this.objCtrl.model.op
      this.apontamentoLotes.codigoComponente = this.objCtrl.model.codigoComponente
      this.apontamentoLotes.derivacaoComponente = this.objCtrl.model.derivacaoComponente
      this.apontamentoLotes.tipoApontamento = 1
      this.apontamentoLotes.tipoDespositivo = 0
      this.apontamentoLotes.usuarioSeniorX = this.api.objCtrl.UserSenior

      this.objCtrl.listLote.filter(lt=>{
        this.objCtrl.listSelected.filter(ls=>{
          if(lt.id == ls.id){
            
            this.apontamentoLotes.lotes.push({
              codigoLote: ls.name,
              codigoDefeito: ls.defeito,
              dataFinal: this.global.getDate(date),
              horaFinal: date.getMinutes() + (date.getHours() * 60)
            })
        
          }
        });
      });

      //Chama servico de apontamento dos lotes
      this.api.generateApontamentoLotes(
        this.api.objCtrl.empresaSelect.id,
        this.api.objCtrl.Tenant,
        this.apontamentoLotes
      ).then((res) => {

        //Atualiza itens da tela com situações de 'perfeito' ou 'defeito'
        this.objCtrl.listLote.filter(lt=>{
          this.objCtrl.listSelected.filter(ls=>{
            if(lt.id == ls.id){               
              if(ls.perfect){
                lt.status = 2;
              }else{
                lt.status = 3;
              }
            }
          });
        });

        //Limpa itens da tela
        this.objCtrl.showSelect = false;
        this.objCtrl.listSelected = [];

        this.global.showToast({type:'success',title:'Sucesso!',text:'Apontamento concluido com sucesso!'});
        
        // verifica se tem mais apontamentos para ser realizado
        let countNaoApontado = 0;
        this.objCtrl.listLote.filter(item=>{
          if(item.status == 1){
            countNaoApontado++;
          }
        });
        if(countNaoApontado <= 0){
          this.verificaIntervaloApontamento();
        }
        
        this.esperarApontamento = false
        resolve()

      })
      .catch((err) => {

        this.esperarApontamento = false

        this.global.showToast({
          type:'error',
          title:'Ocorreu um erro',
          text: err.erroExecucao,
          position:'center',
          life:12000
        }); 

        reject()

      })

    })

  }

  verificaDateFilter(){
    let ini=true,fim=true;
    return new Promise((resolve, reject) => {
      if(this.objCtrl.model.dtini == null){
        this.global.showToast({type:'error',title:'Campo Data Início',text:'O campo não foi informado, favor informar data de início!'});
        ini=false;
      }
      if(this.objCtrl.model.dtfim == null){
        this.global.showToast({type:'error',title:'Campo Data Fim',text:'O campo não foi informado, favor informar data de fim!'});
        fim=false;
      }
      if(!ini || !fim){
        reject();
      }else{
        resolve();
      }
    });
  }

  verificaRangeLotes(){
    let de=true,ate=true;
    return new Promise((resolve, reject) => {
      if(this.objCtrl.model.lotede == 0){
        this.global.showToast({type:'error',title:'Campo Lote Início',text:'O campo não foi informado, favor informar o início do lote!'});
        de=false;
      }
      if(this.objCtrl.model.loteate == 0){
        this.global.showToast({type:'error',title:'Campo Lote Fim',text:'O campo não foi informado, favor informar o fim do lote!'});
        ate=false;
      }
      if(!de || !ate){
        reject();
      }else{
        resolve();
      }
    });
  }

  verificaApontamentoIniciado() {
    
    return new Promise((resolve, reject) => {
      if(!this.apontamentoIniciado) {
        this.global.showToast({type:'error',title:'Botão Inicio',text:'O apontamento não foi iniciado.'})
        reject() 
      } else {
        resolve()
      }
    })
    
  }

  verificaDefeitoNaoApontado(){
    return new Promise((resolve,reject)=>{
      let naoApontado = false;
      this.objCtrl.listSelected.filter(item => {
        if(item.perfect == false && item.defeito == null){
          naoApontado = true;
        }
      });
      if(naoApontado){
        this.global.showToast({
          type: 'error',
          title: 'Atenção',
          text: 'É obrigatório especificar o defeito dos lotes selecionados'
        });
        reject();
      }else{
        resolve();
      }
    });
  }

  verificaIntervaloApontamento(){
    
    if(this.objCtrl.listSelected.length > 0){
      this.objCtrl.showSelect = true;
    }else{
      let countNaoApontado = 0;
      this.objCtrl.listLote.filter(item=>{
        // 1 => nao paontado | 2 => apontado Perfeito | 3 => apontado Defeito
        if(item.status == 1){
          countNaoApontado++;
        }
      });
      if(countNaoApontado > 0){
        this.global.showToast({
          type: 'warn',
          title: 'Atenção',
          text: 'Não existe apontamentos para serem realizados nesse intervalo de lotes.'
        });
      }else{
        this.objCtrl.camposApontamento = false;
        this.objCtrl.model.lotede = 0;
        this.objCtrl.model.loteate = 0;

        this.global.showConfirm({
          header: 'Alerta',
          message: 'Não existem mais apontamento para ser realizado!\nDeseja voltar para a página: Menu Op\'s?',
          icon: 'fas fa-bell text-_warn'
        }).then(res =>{
          if( res == true ){
            this.router.navigate([`menuop/${this.lastpage}/${this.CodOri}`]);
          }
        });
      }
      
    }
  }
 
  apontarInicioFim(opcao) {

    return new Promise((resolve,reject)=>{
        
      //variavel para recurso de data e hora local
      let date = new Date()
      
      if(!this.visaoMobile && opcao === 'inicio') {
        date = this.objCtrl.model.dtini
      } else if (!this.visaoMobile && opcao === 'fim') {
        date = this.objCtrl.model.dtfim
      }

      let hora = date.getMinutes() + (date.getHours() * 60)

      //paramentros para inicio do apontamento
      this.apontamentoInicioFim.cadastroOperador = 0
      this.apontamentoInicioFim.codigoEmpresa = this.api.objCtrl.empresaSelect.id
      this.apontamentoInicioFim.codigoOrigem = this.CodOri
      this.apontamentoInicioFim.numeroOP = this.objCtrl.model.op
      this.apontamentoInicioFim.codigoComponente = this.objCtrl.model.codigoComponente
      this.apontamentoInicioFim.derivacaoComponente = this.objCtrl.model.derivacaoComponente
      this.apontamentoInicioFim.dataApontamento = this.global.getDate(date)
      this.apontamentoInicioFim.horaApontamento = hora
      this.apontamentoInicioFim.tipoDespositivo = 0;
      this.apontamentoInicioFim.usuarioSeniorX = this.api.objCtrl.UserSenior

      //console.log(this.objCtrl)

      if(opcao === 'fim') {

        if(this.objCtrl.listSelected.length > 0) {
          this.global.showToast({
            type: 'warn',
            title: 'Atenção',
            text: 'Registre, ou cancele, os apontamentos abaixo antes de finalizar.'
          });  
        } else {
          this.apontamentoInicioFim.tipoApontamento = 2
        }

      } else if (opcao === 'inicio') {
        this.apontamentoInicioFim.tipoApontamento = 1
      }

      if(this.apontamentoInicioFim.tipoApontamento != 0) {

        this.api.generateApontamentoInicioFim(this.api.objCtrl.empresaSelect.id,this.api.objCtrl.Tenant,this.apontamentoInicioFim)
        .then((res) => {
          if(this.apontamentoInicioFim.tipoApontamento === 1) {
            this.apontamentoIniciado = true
          } else {
            this.apontamentoIniciado = false  

            if(this.visaoMobile) {
              this.global.showToast({type:'success',title:'Sucesso!',text:'Apontamento Finalizado!'});  
            }   
          }
          resolve()
        })
        .catch((err) => {

          //console.log(err)

          this.global.showToast({
            type:'error',
            title:'Ocorreu um erro',
            text: err.erroExecucao,
            position:'center',
            life:12000
          });  

          reject()

        })
        
      }

    })
  
  }

  // apontarFim() {
    
  //   if(this.objCtrl.listSelected.length > 0) {
  //     this.global.showToast({
  //       type: 'warn',
  //       title: 'Atenção',
  //       text: 'Registre, ou cancele, os apontamentos abaixo antes de finalizar.'
  //     });  
  //   } else {
  //     this.apontamentoIniciado = false
  //   }

  // }

  showVariables(){
    console.group('Variables');
      console.log('objCtrl: ', this.objCtrl, '-------------------');
      console.log('listLote',this.objCtrl.listLote);
      console.log('listLoteate',this.objCtrl.listLoteate);
      console.log('listSelected',this.objCtrl.listSelected);
      console.log('listOptDefeitos',this.objCtrl.listOptDefeitos);
      console.log('showSelect',this.objCtrl.showSelect);
      console.log('model',this.objCtrl.model);
    console.groupEnd();
  }


}




interface listops{
  id: string | number,
  name: string,
  perfect: boolean, //status
  defeito: string | number
}