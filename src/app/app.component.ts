import { Component } from "@angular/core";
import { ApiService } from "./services/api/api.service";
import { GlobalService } from "./services/global/global.service";
import { Router } from "@angular/router";

import { AppService } from "./senior/services/app.service";
import { UserData } from "./senior/user-data";
import { reject } from "q";


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  providers:[AppService]
})
export class AppComponent {
  title = "app";
  connectSenior = false;

  constructor(
    private router: Router,
    public api: ApiService,
    public global: GlobalService,
    public appService: AppService
  ){  
    
    if(this.global.callSenior){
      this.getInfoSeniorX().then(res=>{
        // carregas a lista de empresas
        this.api.saveDateSeniorX(res).then(res1=>{
          this.api.autenticaMD().then(resMD=>{ // busca token e guarda na aplicação
            // if(this.global.callSenior == false){
            //   console.log('getToken',this.api.getToken());
            // }
            this.api.autenticaSeniorX().then(res=>{
              // habilita redirecionar para a pagina principal: origens
              this.connectSenior = true;
              if (! this.global.canActivate()) {
                if (window.location.pathname == "/origens") {
                  // nao da redirect para a pagina origens
                } else {
                  this.router.navigate(['origens']);
                }
              }
            });
          });
        });
      });
    }else{
      // this.api.getEmpresas().then(res=>{
      let res = {
        bloqueado: false,
        email: "THYMOTEO@SENIOR-ES.COM.BR",
        id: "e824a41f-04e2-4801-9882-d6cb5757cbdb",
        localidade: "pt-BR",
        nome: "thymoteo.martins",
        nomeCompleto: "Thymoteo Martins",
        tenantDomain: "senior-es-dev.com.br",
        tenantLocale: "pt-BR",
        tenantName: "senior-es-dev"
      }
      this.api.saveDateSeniorX(res).then(res1=>{

        this.api.autenticaMD().then(resMD=>{ // busca token e guarda na aplicação
          // if(this.global.callSenior == false){
          //   console.log('getToken',this.api.getToken());
          // }
          
          this.api.autenticaSeniorX().then(res=>{
            // habilita redirecionar para a pagina principal: origens
            this.connectSenior = true;

            // teste andré
            //api.empresa$.subscribe((newBool: boolean) => {
            //  alert(" app.component recebeu notificação:" + api.objCtrl.empresaSelect.id);
            //});
            // teste andré

            if (! this.global.canActivate()) {
              if (window.location.pathname == "/origens" || 
              // pages Cadastros
              window.location.pathname == "/cadastro/perfil" || window.location.pathname == "/cadastro/relacionamento") {
                // nao da redirect para a pagina origens
              } else {
                this.router.navigate(['origens']);
              }
            }
          });
          
        });
      });
    }
    
  }


  getInfoSeniorX(){
    return new Promise((resolve, reject)=>{
      this.appService.getDadosUsuario().subscribe(data => {
        //console.log('getInfoSeniorX',data);
        resolve(data);
      });
    });
  }


}
